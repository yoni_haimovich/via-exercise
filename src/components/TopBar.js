import React from 'react';
import SearchBox from './SearchBox';
import './TopBar.css';

export default () => (
  <div className="topBarContainer">
    <div className="title">Contact List</div>
    <div className="searchBoxWrapper"><SearchBox /></div>
  </div>
);
