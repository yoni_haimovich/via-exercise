import React from 'react';
import Contact from './Contact';
import './ContactList.css';
import generateUuid from '../utils/uuidGenerator';
import beautifyPhoneNumber from '../utils/phoneNumberBeautifier';

class ContactList extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = { isLoading: false, contacts: [], error: null };

    this.fetchContacts = this.fetchContacts.bind(this);
  }

  async componentDidMount() {
    await this.fetchContacts();
  }

  async fetchContacts() {
    this.setState({ isLoading: true });

    try {
      const response = await fetch('http://private-05627-frontendnewhire.apiary-mock.com/contact_list');

      if (!response.ok) {
        throw new Error('something went wrong');
      }

      const data = await response.json();

      this.setState({ isLoading: false, contacts: data, error: null });
    } catch (e) {
      this.setState({ isLoading: false, contacts: [], error: e });
    }
  }

  render() {
    if (this.state.isLoading) {
      return <div>Loading...</div>;
    }

    if (this.state.error) {
      return <div>{`error: ${this.state.error.message}`}</div>;
    }

    /*
      adding a few dummy contacts as a workaround for the flex issue described here:
      https://stackoverflow.com/questions/18744164/flex-box-align-last-row-to-grid
    */
    return (
      <div className="contactListContainer">
        {this.state.contacts.map(contact => <Contact
          key={generateUuid()}
          name={contact.name}
          driverRank={contact.driverRank}
          driverType={contact.driverType.toLowerCase().trim()}
          phoneNumber={contact.phone ? beautifyPhoneNumber(contact.phone) : contact.phone}
          email={contact.email}
          image={contact.profile_image || 'https://dummyimage.com/300x300/000/fff.png&text=no+image'}
          />)}
          <Contact isDummy />
          <Contact isDummy />
          <Contact isDummy />
          <Contact isDummy />
      </div>
    );
  }
}

export default ContactList;
