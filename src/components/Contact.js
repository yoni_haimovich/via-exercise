import React from 'react';
import citizen from '../assets/citizen.svg';
import professional from '../assets/professional.svg';
import './Contact.css';

class Contact extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { isHovering: false };
  }

  getDriverIcon(driverType) {
    const driverIcon = { citizen, professional };
    return driverIcon[driverType];
  }

  render() {
    const { isDummy, name, driverRank, driverType, phoneNumber, email, image } = this.props;

    if (isDummy) {
      return <div className="contactContainer dummyContact" />
    }

    return (
      <div className="contactContainer" onMouseEnter={() => this.setState({isHovering: true})} onMouseLeave={() => this.setState({isHovering: false})}>
        <div
          className="contactImage"
          style={{
            backgroundImage: `url(${image})`,
            backgroundSize: 'cover',
            height: this.state.isHovering ? '160px' : '200px',
          }}
          />
        <img className="contactIndicatorIcon" src={this.getDriverIcon(driverType)} alt="contactIndicatorIcon" />
        <div className="contactText contactTitle">{name}</div>
        <div className="contactText">{`Driver rank: ${driverRank}`}</div>
        { this.state.isHovering && phoneNumber ? <div className="contactText">{`Phone Number: ${phoneNumber}`}</div> : null }
        { this.state.isHovering && email ? <div className="contactText">{`Email: ${email}`}</div> : null }
      </div>
    );
  }
}

export default Contact;
