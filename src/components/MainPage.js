import React from 'react';
import TopBar from './TopBar';
import ContactList from './ContactList';
import './MainPage.css';

export default () => (
  <div className="mainPageContainer">
    <TopBar />
    <div className="mainPageBody">
      <ContactList />
    </div>
  </div>
);
