import React from 'react';
import './SearchBox.css';

export default () => <input className="searchBox" type="text" placeholder="search..." />;
