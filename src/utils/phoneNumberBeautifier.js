export default phoneNumberString => {
  const onlyDigits = phoneNumberString.replace(/\D/g, '');
  return [onlyDigits.slice(0, 3), onlyDigits.slice(3, 6), onlyDigits.slice(6)].join('.');
}
